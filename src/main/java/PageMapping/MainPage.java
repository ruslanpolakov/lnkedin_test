package PageMapping;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

public class MainPage {
    private WebDriver driver;
    public ProfileMenu profileMenu;

    public MainPage(WebDriver driver) {
        this.driver = driver;
        profileMenu = new ProfileMenu();
    }

    private By WelcomeMessage = By.xpath("//a[@data-control-name='identity_welcome_message']");
    private By Profile_icon = By.id("profile-nav-item");

    public void Assert_PageDisplayed()
    {
        Assert.assertTrue(driver.findElement(Profile_icon).isDisplayed(),"Profile menu not displayed");
        Assert.assertTrue(driver.findElement(WelcomeMessage).isDisplayed(),"Welcome message not displayed");
    }

    public class ProfileMenu
    {
        private By LogOut_menuItem = By.xpath(".//li[@class='nav-dropdown__action']//a[contains(@href,'logout')]");

        private WebElement SignOut_menuItem()
        {
            return driver.findElement(Profile_icon).findElement(LogOut_menuItem);
        }

        public LoginPage SignOut()
        {
            driver.findElement(Profile_icon).click();
            SignOut_menuItem().click();
            return new LoginPage(driver);
        }
    }
}
