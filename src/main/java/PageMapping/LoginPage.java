package PageMapping;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LoginPage {
    private WebDriver driver;
    private WebDriverWait wait;

    public LoginPage(WebDriver driver)
    {
        this.driver=driver;
        this.wait = new WebDriverWait(driver,30);
        wait.until(ExpectedConditions.visibilityOfElementLocated(Login_button));
    }

    private By Login_field = By.id("login-email");
    private By Password_field = By.id("login-password");
    private By Login_button = By.id("login-submit");

    public MainPage LogIn(String User, String Password)
    {
        driver.findElement(Login_field).clear();
        driver.findElement(Login_field).sendKeys(User);
        driver.findElement(Password_field).clear();
        driver.findElement(Password_field).sendKeys(Password);
        driver.findElement(Login_button).click();
        return new MainPage(driver);
    }

    public LoginPage LogIn_WithoutNavigation(String User, String Password)
    {
        driver.findElement(Login_field).clear();
        driver.findElement(Login_field).sendKeys(User);
        driver.findElement(Password_field).clear();
        driver.findElement(Password_field).sendKeys(Password);
        driver.findElement(Login_button).click();
        return new LoginPage(driver);
    }

    public void Assert_PageDisplayed()
    {
        Assert.assertTrue(driver.findElement(Login_button).isDisplayed(),"Login button not displayed");
    }
}
