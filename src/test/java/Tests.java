import PageMapping.LoginPage;
import PageMapping.MainPage;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.*;

public class Tests {
    private WebDriver driver;
    LoginPage loginPage;

    @Parameters({"browser", "URL", "WaitTime"})
    @BeforeMethod
    public void SetUp(String browser, String URL, int WaitTime) throws Exception
    {
        driver = Driver.GerDiver(browser,URL,WaitTime);
        loginPage=new LoginPage(driver);
    }

    @AfterMethod
    public void Close()
    {
        driver.quit();
    }

    @Parameters({"User", "Password"})
    @Test
    public void LogIn(String User, String Password)
    {
        //login and assert main page displayed
        MainPage mainPage=loginPage.LogIn(User,Password);
        mainPage.Assert_PageDisplayed();
        //logout and assert LogIn page displayed
        mainPage.profileMenu.SignOut().
                Assert_PageDisplayed();
    }

    @Parameters({"User"})
    @Test
    public void LogIn_WithoutPassword(String User)
    {
        //Attempt to login without password, assert page still LogIn
        loginPage.LogIn_WithoutNavigation(User,"").
                Assert_PageDisplayed();
    }
}
