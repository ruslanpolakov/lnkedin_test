This project is short example of Java/Selenium/Gradle/TestNG framework

Before execution should be done configuration in file src\test\resources\testng.xml :
    parameter "browser" set value to "IE", "FF" or "Chrome"
    parameter "User" set value to your LinkedIn login username
    parameter "Password" set value to your LinkedIn login password

test could be executed from command line using "gradlew.bin clean test" or by run project from IDE

Used PageObject patten, mapping files located in src\main\java\PageMapping
Test cases located src\test\java\Tests.java (simple login-logout validation and incorrect password testcases)

Required WebDriver binaries will be automatically downloaded and setup during execution (used https://github.com/bonigarcia/webdrivermanager)
